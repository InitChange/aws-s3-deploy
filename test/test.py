import os
from random import randint
import shutil

import requests

from bitbucket_pipes_toolkit.test import PipeTestCase


S3_BUCKET = f"bbci-pipes-test-s3-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
S3_BUCKET_TEST = f"{S3_BUCKET}/test"


class S3DeployTestCase(PipeTestCase):

    def setUp(self):
        super().setUp()
        self.random_number = randint(0, 32767)
        self.local_dir = f'aws-s3-deploy-test-{self.random_number}'
        os.mkdir(self.local_dir)

        self.filename = f'{self.local_dir}/deployment-{self.random_number}.txt'

        with open(self.filename, 'w+') as deployment_file:
            deployment_file.write('Pipelines is awesome!')

    def tearDown(self):
        shutil.rmtree(self.local_dir, ignore_errors=True)

    def test_upload_to_s3_bucket(self):
        region = os.getenv('AWS_DEFAULT_REGION', 'us-east-1')

        self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': region,
            'S3_BUCKET': S3_BUCKET_TEST,
            'ACL': "public-read",
            'LOCAL_PATH': self.local_dir
        })

        response = requests.get(f'https://{S3_BUCKET}.s3.{region}.amazonaws.com/test/deployment-{self.random_number}.txt')
        self.assertIn('Pipelines is awesome!', response.content.decode())

    def test_should_fail_if_local_path_doent_exist(self):

        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'S3_BUCKET': S3_BUCKET_TEST,
            'ACL': "public-read",
            'LOCAL_PATH': 'not_exist_local_path'
        })

        self.assertIn('not_exist_local_path directory doesn\'t exist', result)

    def test_should_fail_if_local_is_not_a_directory(self):

        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_DEFAULT_REGION': os.getenv('AWS_DEFAULT_REGION', 'us-east-1'),
            'S3_BUCKET': S3_BUCKET_TEST,
            'ACL': "public-read",
            'LOCAL_PATH': self.filename
        })

        self.assertIn(f'{self.filename} directory doesn\'t exist', result)
